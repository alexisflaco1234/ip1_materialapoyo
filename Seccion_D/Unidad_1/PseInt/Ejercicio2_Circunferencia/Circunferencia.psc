//Pseudocodigo circunferencia

Proceso Circulo
    Definir radio,superficie,perimetro como Real
    Imprimir "Introduce el radio de la circunferencia:"
    Leer radio
    superficie <- PI * radio ^ 2
    perimetro <- 2 * PI * radio
    Imprimir "La superficie es ",superficie
    Imprimir "El perímetro es ",perimetro
FinProceso


//Actividad:
//REQUISITO: Terminar las actividades expuestas en el archivo Circunferencia.alg en esta misma carpeta.
/*
    1. Encuentre que hace falta en el pseudocodigo anterior y corrija, de la misma forma corrija el diagrama correspondiente (Ver imagen).
    2. Escriba el pseudocodigo correspondiente a 2 variaciones de algoritmos que haya planteado.
*/