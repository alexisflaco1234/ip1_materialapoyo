Algoritmo NumerosPrimos
	Escribir "Ingrese un valor para saber la cantidad de valores primos de 0 hasta el numero ingresado: "
	Leer numero 
	
	Para i<-2 Hasta numero Hacer
		m <- 2
		bandera <- Verdadero
		
		Mientras bandera = Verdadero y m < i Hacer
			Si i % m = 0 Entonces
				bandera <- Falso
			SiNo
				m <- m+1 
			FinSi
		FinMientras
		
		Si bandera = Verdadero Entonces
			Escribir "El numero ", i, " es primo"
		FinSi
		
	Fin Para
FinAlgoritmo
